# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.

select title
from movies
where budget > 10000000 and rating < 6;

# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.

select title,movies.movie_id
from movies join genres on movies.movie_id = genres.movie_id
where rating > 8.0 and year > 2000 and genres.genre_name="Action";

# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.

select title 
from movies 
where duration > 150 and oscars >2  and movie_id in 
(select movie_id
 from genres
 where genre_name="Drama");
 

# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.

select title 
from movies
where oscars > 2 and movie_id in 

(select movie_id
from movie_stars join stars on movie_stars.star_id = stars.star_id
where star_name = "Orlando Bloom"  and movie_id in
	(select movie_id
	from movie_stars join stars on movie_stars.star_id = stars.star_id
	where star_name = "Ian McKellen" ));



# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.	 
/*select title
from movies , movie_directors , directors
where movies.votes > 500000 and movies.year < 2000 and movies.movie_id = movie_directors.movie_id and movie_directors.director_id = directors.director_id
and directors.director_name = "Quentin Tarantino"*/

select title
from movies
where movies.movie_id in ( select movie_id
from movie_directors NATURAL JOIN directors
where directors.director_name = "Quentin Tarantino"
);

# 6. Show the thriller films whose budget is greater than 25 million$.	 

select title
from movies NATURAL JOIN genres
where genre_name = "Thriller" and budget > 25000000 ;

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	

/*select title
from movies NATURAL JOIN genres,languages
where genre_name = "Drama" and language_name = "Italian" and movies.year > 1990 and movies.year < 2000;*/

select title
from movies
where year between 1990 and 2000 and movie_id in (select languages.movie_id from languages natural join genres where language_name="Italian" and genre_name="drama");
 
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 

select movie_id , star_name
from movie_stars natural join stars
where star_name like "%Hanks";


# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.

select title,movie_id
from movies
where movie_id in
(select movie_id 
from genres natural join countries
where genre_name = "History" and countries.country_name = "USA") and duration between 100 and 200;
 
# 10.Compute the average budget of the films directed by Peter Jackson.

# 11.Show the Francis Ford Coppola film that has the minimum budget.

# 12.Show the film that has the most vote and has been produced in USA.
