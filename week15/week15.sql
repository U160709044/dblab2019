insert into movies(movie_id,title,ranking,rating,year,votes,duration,oscars,budget)
select distinct movie_id, title,ranking,rating,year,votes,duration,oscars,budget)
from denormalized;

insert into languages(movie_id,language_name)
select distinct movie_id ,language_name
from denormalized

insert into genres(movie_id,genre_name)
select distinct producer_country_id,producer_country_name
from denormalized
union
select distinct director_country_id, director_country_name
from denormalized
union
select distinct start_country_id,star_country_name
from denormalized;

insert into stars(stars_id,country_id,star_name)
select distinct star_id,star_country_id,star_name
from denormalized;