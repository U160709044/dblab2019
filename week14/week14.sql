load data infile "/var/lib/mysql-files//movies.csv"
into table movies fields terminated by "," enclosed by '"';
load data infile "/var/lib/mysql-files/countries.csv"
into table countries fields terminated by ",";
load data infile "/var/lib/mysql-files/stars.csv"
into table stars fields terminated by ",";
load data infile "/var/lib/mysql-files/movie_stars.csv"
into table movie_stars fields terminated by ",";
load data infile "/var/lib/mysql-files/directors.csv"
into table directors fields terminated by ",";
load data infile "/var/lib/mysql-files/movie_directors.csv"
into table movie_directors fields terminated by ",";
load data infile "/var/lib/mysql-files/genres.csv"
into table genres fields terminated by ",";
load data infile "/var/lib/mysql-files/languages.csv"
into table languages fields terminated by ",";
load data infile "/var/lib/mysql-files/producer_countries.csv"
into table producer_countries fields terminated by ",";
