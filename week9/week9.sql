create view usa_customers as
select CustomerID, CustomerName, ContactName
from Customers
where country = "USA";

select * from usa_customers join Orders on usa_customers.CustomerID = Orders.CustomerID ;

select avg(price) from Products;

create or replace view products_below_avg_price  as 
select ProductID,ProductName, Price
from Products
where Price < (select avg(Price) from Products);

select *
from usa_customers join Orders on usa_customers.CustomerID = Orders.CustomerID
where orderID in (select orderID from OrderDetails join products_below_avg_price on OrderDetails.ProductID = products_below_avg_price.ProductID);

drop view usa_customers;